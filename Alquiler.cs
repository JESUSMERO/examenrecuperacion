﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecuperacionMauro
{
    public class Alquiler : barco

    {


        public DateTime fechainical { get; set; }
        public DateTime fechafinal { get; set; }
        public double SueldoBase { get; set; }
        public double ComplementoAnual { get; set; }

        public int Total { get; set; }

        public double Sueldo { get; set; }

        public Alquiler()
        {

            this.SueldoBase = 0.8;
            this.ComplementoAnual = 0.3;


        }

        //creamos nuestra funcion
        public int CalcularSueldo()
        {
            TimeSpan timeSpan = DateTime.Today - this.fechainical;
            int dias = timeSpan.Days;
            int años = dias / 365;
            return años;


        }
    }
    }
