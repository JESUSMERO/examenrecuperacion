﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecuperacionMauro
{
    //agregamos la herencia de la clase alquiler
    class AlquilerFijo:Alquiler
    {
        public DateTime FechaIngreso { get; set; }
        public DateTime FechaSalida { get; set; }
        public AlquilerFijo()
        {
        }
        public double CalcularSueldoCobrar()
        {
            return 0.8;
        }
    }
}
