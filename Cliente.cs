﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecuperacionMauro
{
    public class Cliente

    {

      
        public Cliente()
        {
        }

        public Cliente(string nombres, string apellidos, int cedulaIdentidad, string clientenuevo)
        {
            Nombres = nombres;
            Apellidos = apellidos;
            CedulaIdentidad = cedulaIdentidad;
            this.clientenuevo = clientenuevo;
        }

        public string Nombres { get; set; }
        public string Apellidos { get; set; }

        public int CedulaIdentidad{ get; set; }

        public string clientenuevo { get; set; }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
    
}
